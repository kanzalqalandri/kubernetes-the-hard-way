# ==============================
#   AWS Key Pair
# ==============================

resource "aws_key_pair" "my_key_pair" {
  key_name   = "kubernetes-the-hard-way"
  public_key = file("/home/kanzal/.ssh/id_rsa.pub")
}


# ================================
#        Master Nodes
# ================================


resource "aws_instance" "controller_instances" {
  count                       = 3
  ami                         = "ami-0f2967bce46537146" # ubuntu 20
  instance_type               = "t3.micro"
  key_name                    = aws_key_pair.my_key_pair.key_name
  subnet_id                   = aws_subnet.my_subnet.id
  associate_public_ip_address = true
  private_ip                  = "10.0.1.1${count.index}"
  root_block_device {
    volume_size = 50
  }
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  tags = {
    Name = "controller-${count.index}"
  }
  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname "controller-${count.index}"
              EOF
}


# =========================
#       Worker Nodes
# =========================


resource "aws_instance" "worker_instances" {
  count                       = 3
  ami                         = "ami-0f2967bce46537146" # ubuntu 20
  instance_type               = "t3.micro"
  key_name                    = aws_key_pair.my_key_pair.key_name
  subnet_id                   = aws_subnet.my_subnet.id
  associate_public_ip_address = true
  private_ip                  = "10.0.1.2${count.index}"
  root_block_device {
    volume_size = 50
  }
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  tags = {
    Name = "worker-${count.index}"
  }
  user_data = <<-EOF
              #!/bin/bash
              hostnamectl set-hostname "worker-${count.index}"
              EOF
}

