# =======================
#        VPC Blok
# =======================


resource "aws_vpc" "my_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "kubernetes-the-hard-way"
  }
}


# =================
#   Subnet Block
# =================

resource "aws_subnet" "my_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "kubernetes-the-hard-way"
  }
}


# =========================
#    Internet Gateway
# =========================

resource "aws_internet_gateway" "my_internet_gateway" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "kubernetes-the-hard-way"
  }
}

# =========================
#    Route Table
# =========================


resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "kubernetes-the-hard-way"
  }
}


# =============================
#    Route Table Association
# =============================

resource "aws_route_table_association" "my_route_table_association" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_route_table.id
}



# =============================
#        AWS Route 
# =============================

resource "aws_route" "my_default_route" {
  route_table_id         = aws_route_table.my_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my_internet_gateway.id
}


# ================================
#        Securty Group & Rules
# ================================

resource "aws_security_group" "my_security_group" {
  name        = "kubernetes-the-hard-way"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}




# ===========================
#   Network Load Balancer
# ===========================


resource "aws_lb" "my_load_balancer" {
  name               = "kubernetes-the-hard-way"
  internal           = false
  load_balancer_type = "network"
  enable_deletion_protection = false
  subnets            = [aws_subnet.my_subnet.id]
}


resource "aws_lb_target_group" "my_target_group" {

  name     = "kubernetes-the-hard-way"
  port     = 6443 
  protocol = "TCP"
  target_type = "ip"
  vpc_id   = aws_vpc.my_vpc.id
}


resource "aws_lb_target_group_attachment" "my_target_group_attachment" {

  for_each = {
    "instance1" = aws_instance.controller_instances[0].private_ip
    "instance2" = aws_instance.controller_instances[1].private_ip
    "instance3" = aws_instance.controller_instances[2].private_ip
  }

  target_group_arn = aws_lb_target_group.my_target_group.arn
  target_id        = each.value
}




resource "aws_lb_listener" "my_listener" {
  load_balancer_arn = aws_lb.my_load_balancer.arn
  port              = 443
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_target_group.arn
  }
}

